IDENTIFICATION DIVISION.
       PROGRAM-ID. SP18202.
       AUTHOR. GABRIEl SILVA.
      **************************************
      * CRUD DE CARGOS *
      **************************************
      *----------------------------------------------------------------
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
                         DECIMAL-POINT IS COMMA.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       SELECT CEPS ASSIGN TO DISK
                    ORGANIZATION IS INDEXED
                    ACCESS MODE  IS DYNAMIC
                    RECORD KEY   IS FORM-CODIGO
                    FILE STATUS  IS ST-ERRO
                    ALTERNATE RECORD KEY IS FORM-DENOMINACAO
                                            WITH DUPLICATES.
      *
      *-----------------------------------------------------------------
       DATA DIVISION.
       FILE SECTION.
       FD CEPS
               LABEL RECORD IS STANDARD
               VALUE OF FILE-ID IS "REGCARGO.DAT".
       01 REGCARGO.    
                03 FORM-CODIGO             PIC 9(9).
                03 FORM-DENOMINACAO         PIC X(20).
				03 FORM-TIPO-SALARIO             PIC X(12).
				03 FORM-NIVEL             PIC X(20).
				03 FORM-SALARIO-BASE                 PIC X(6).
                03 FORM-TIPO-RISCO          PIC X(12).
                03 FORM-SITUACAO               PIC X(12).
               
      *
      *-----------------------------------------------------------------
       WORKING-STORAGE SECTION.
       77 W-SEL        PIC 9(01) VALUE ZEROS.
       77 W-CONT       PIC 9(06) VALUE ZEROS.
       77 ST-ERRO      PIC X(02) VALUE "00".
       77 W-ACT        PIC 9(02) VALUE ZEROS.
       77 LIMPA        PIC X(50) VALUE SPACES.

      *
       01 TABNIVEL.
          03 T1 PIC X(56) VALUE
          "PRESIDENCIA   DIRETORIA     GERENCIA      SUPERVISAO    ".
          03 T2 PIC X(56) VALUE 
          "COORDENACAO   OPERACIONAL   ADMINISTRATIVOTECNICO       ".
          03 T3 PIC X(56) VALUE
          "CONSULTORIA   ASSESSORIA".

       01 TABAUX REDEFINES TABNIVEL.
           03 TDA              PIC X(13) OCCURS 9 TIMES. 
      *
       01 TABRISCO.
          03 T1 PIC X(45) VALUE
          "BAIXO    ALTO     MEDIO    GRAVE    SEM RISCO".

       01 TABAUX REDEFINES TABRISCO.
           03 TDA              PIC X(9) OCCURS 5 TIMES. 

        01 TABTIPOSALARIO.
          03 T1 PIC X(48) VALUE
          "HORISTA     MENSALISTA  DIARISTA    COMISSIONADO".

       01 TABAUX REDEFINES TABTIPOSALARIO.
           03 TDA              PIC X(12) OCCURS 4 TIMES. 
      *---------------------------------------------------------
       SCREEN SECTION.
       01  CADASTRO DE CARGO.
           05  LINE 01  COLUMN 01 
               VALUE  "   CADASTRO DE CARGOS".
           05  LINE 02  COLUMN 01 
               VALUE  "CODIGO".
           05  LINE 03  COLUMN 01 
               VALUE  "DENOMINACAO".
           05  LINE 04  COLUMN 01 
               VALUE  "TIPO SALARIO".
           05  LINE 05  COLUMN 01 
               VALUE  "NIVEL".
           05  LINE 06  COLUMN 01 
               VALUE  "SALARIO BASE".
           05  LINE 07  COLUMN 01 
               VALUE  "TIPO RISCO".
           05  LINE 08  COLUMN 01 
               VALUE  "SITUACAO".
           05  CODIGO
               LINE 02  COLUMN 08  PIC 9(04)
               USING  FORM-CODIGO.
           05  DENOMINACAO
               LINE 03  COLUMN 13  PIC X(20)
               USING  FORM-DENOMINACAO.
           05  TIPOSALARIO
               LINE 04  COLUMN 14  PIC X(01)
               USING  FORM-TIPO-SALARIO.
           05  TIPOSALARIODESC
               LINE 04  COLUMN 16  PIC X(12)
               USING  FORM-TIPO-SALARIO-DESC.
           05  NIVEL-ID
               LINE 05  COLUMN 07  PIC 9(01)
               USING  FORM-ID-NIVEL.
           05  NIVELDESC
               LINE 05  COLUMN 09  PIC X(15)
               USING  FORM-NIVEL-DESC.
           05  SALARIO-BASE
               LINE 06  COLUMN 14  PIC 9(6)
               USING  FORM-SALARIO-BASE.
           05  TIPORISCO
               LINE 07  COLUMN 12  PIC X(01)
               USING  FORM-TIPO-RISCO-ID.
           05  LINE 07  COLUMN 14  PIC X(12)
               USING  FORM-RISCO-DESC.
           05  SITUACAOID
               LINE 08  COLUMN 10  PIC X(01)
               USING  FORM-ID-SITUACAO.
           05  SITUACAODESC
               LINE 08  COLUMN 12  PIC X(12)
               USING  FORM-SITUACAO-DESC.

      *-----------------------------------------------------------------
       PROCEDURE DIVISION.
       INICIO.
      *
       INC-OP0.
           OPEN I-O REGCARGO
           IF ST-ERRO NOT = "00"
               IF ST-ERRO = "30"
                      OPEN OUTPUT '    REGCARGO'
                      MOVE "*** CRIANDO ARQUIVO... **" TO MENS
                      PERFORM ROT-MENSAGEM THRU ROT-MENSAGEMAGEM-FIM
                      CLOSE REGCARGO
                      GO TO INICIO
                   ELSE
                      MOVE "ERRO AO TENTAR ABRIR ARQUIVO" TO MENS
                      PERFORM ROT-MENSAGEM THRU ROT-MENSAGEMAGEM-FIM
                      GO TO ROT-FIM
                ELSE
                    NEXT SENTENCE.
       INC-001.
                MOVE ZEROS  TO FORM-CODIGO
				               FORM-REGIAO
                MOVE SPACES TO FORM-DENOMINACAO
               				   FORM-TIPO-SALARIO
							   FORM-NIVEL
                               FORM-SALARIO-BASE
							    FORM-TIPO-RISCO
							   FORM-LATITUDE
							   FORM-REGIAO						   
                DISPLAY TELA.
       