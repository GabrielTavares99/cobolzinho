       IDENTIFICATION DIVISION.
       PROGRAM-ID. SP18202.
       AUTHOR. GABRIEl SILVA.
      **************************************
      * CRUD DE CARGOS *
      **************************************
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
                         DECIMAL-POINT IS COMMA.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       SELECT REGCARGO ASSIGN TO DISK
                    ORGANIZATION IS INDEXED
                    ACCESS MODE  IS DYNAMIC
                    RECORD KEY   IS FORM-CODIGO
                    FILE STATUS  IS ST-ERRO
                    ALTERNATE RECORD KEY IS FORM-DENOMINACAO
                                            WITH DUPLICATES.
      *
      *-----------------------------------------------------------------
       DATA DIVISION.
       FILE SECTION.
       FD REGCARGO
               LABEL RECORD IS STANDARD
               VALUE OF FILE-ID IS "REGCARGO.DAT".
       01 CARGO.    
                03 FORM-CODIGO               PIC 9(04).
                03 FORM-DENOMINACAO          PIC X(20).
				03 FORM-TIPO-SALARIO-ID      PIC 9(01).
				03 FORM-TIPO-SALARIO-DESC    PIC X(12).
                03 FORM-NIVEL-ID             PIC 9(01).
				03 FORM-NIVEL-DESCRICAO      PIC X(15).
                03 FORM-SALARIO-BASE         PIC 9(06).
                03 FORM-TIPO-RISCO-ID        PIC X(01).
                03 FORM-TIPO-RISCO-DESC      PIC X(12).
                03 FORM-SITUACAO-ID          PIC X(01).
                03 FORM-SITUACAO-DESC        PIC X(12).
               
      *
      *-----------------------------------------------------------------
       WORKING-STORAGE SECTION.
       77 W-SELECAO        PIC 9(001) VALUE ZEROS.
       77 W-CONT           PIC 9(006) VALUE ZEROS.
       77 ST-ERRO          PIC X(002) VALUE "00".
       77 W-OPC            PIC 9(02)  VALUE ZEROS.
       77 LIMPA            PIC X(70) VALUE SPACES.
       77 W-MENSAGEM       PIC X(120) VALUE SPACES.
       77 W-ACAO           PIC 9(001) VALUE ZEROS.

      *
       01 TABNIVEL.
          03 T1 PIC X(56) VALUE
          "PRESIDENCIA   DIRETORIA     GERENCIA      SUPERVISAO    ".
          03 T2 PIC X(56) VALUE 
          "COORDENACAO   OPERACIONAL   ADMINISTRATIVOTECNICO       ".
          03 T3 PIC X(56) VALUE
          "CONSULTORIA   ASSESSORIA".

       01 TABAUX REDEFINES TABNIVEL.
           03 TDA              PIC X(13) OCCURS 9 TIMES. 
      *
       01 TABRISCO.
          03 T1 PIC X(45) VALUE
          "BAIXO    ALTO     MEDIO    GRAVE    SEM RISCO".

       01 TABAUX REDEFINES TABRISCO.
           03 TDA              PIC X(9) OCCURS 5 TIMES. 

        01 TABTIPOSALARIO.
          03 T1 PIC X(48) VALUE
          "HORISTA     MENSALISTA  DIARISTA    COMISSIONADO".

       01 TABAUX REDEFINES TABTIPOSALARIO.
           03 TDA              PIC X(12) OCCURS 4 TIMES. 
      *---------------------------------------------------------
       SCREEN SECTION.
       01  TELA.
           05  LINE 01  COLUMN 01 
               VALUE  "=-=-=-=-=-=-=-=-=-= CADASTRO DE CARGOS =".
           05  LINE 01  COLUMN 41 
               VALUE  "=-=-=-=-=-=-=-=-=-=".
           05  LINE 02  COLUMN 01 
               VALUE  " Codigo".
           05  LINE 03  COLUMN 01 
               VALUE  " Denominacao".
           05  LINE 04  COLUMN 01 
               VALUE  " Tipo Salario".
           05  LINE 05  COLUMN 01 
               VALUE  " Nivel".
           05  LINE 06  COLUMN 01 
               VALUE  " Salario Base".
           05  LINE 07  COLUMN 01 
               VALUE  " Tipo Risco".
           05  LINE 08  COLUMN 01 
               VALUE  " Situacao".
           05  CODIGO
               LINE 02  COLUMN 09  PIC 9(04)
               USING  FORM-CODIGO.
           05  DENOMINACAO
               LINE 03  COLUMN 14  PIC X(20)
               USING  FORM-DENOMINACAO.
           05  TIPO-SALARIO
               LINE 04  COLUMN 15  PIC 9(01)
               USING  FORM-TIPO-SALARIO-ID.
           05  TIPO-SALARIO-DESC
               LINE 04  COLUMN 17  PIC X(12)
               USING  FORM-TIPO-SALARIO-DESC.
           05  NIVEL-ID
               LINE 05  COLUMN 08  PIC 9(01)
               USING  FORM-NIVEL-ID.
           05  NIVEL-DESCRICAO
               LINE 05  COLUMN 10  PIC X(15)
               USING  FORM-NIVEL-DESCRICAO.
           05  SALARIO-BASE
               LINE 06  COLUMN 15  PIC 9(06)
               USING  FORM-SALARIO-BASE.
           05  TIPO-RISCO
               LINE 07  COLUMN 13  PIC X(01)
               USING  FORM-TIPO-RISCO-ID.
           05  TIPO-RISCO-DESC
               LINE 07  COLUMN 15  PIC X(12)
               USING  FORM-TIPO-RISCO-DESC.
           05  SITUACAO-ID
               LINE 08  COLUMN 11  PIC X(01)
               USING  FORM-SITUACAO-ID.
           05  SITUACAO-DESC
               LINE 08  COLUMN 13  PIC X(12)
               USING  FORM-SITUACAO-DESC.

      *-----------------------------------------------------------------
       PROCEDURE DIVISION.
       INICIO.
      *
       INC-OP0.
           OPEN I-O REGCARGO
           IF ST-ERRO NOT = "00"
               IF ST-ERRO = "30"
                      OPEN OUTPUT REGCARGO
                      MOVE "*** CRIANDO ARQUIVO... **" TO W-MENSAGEM
                      PERFORM ROT-MENSAGEM THRU ROT-MENSAGEM-FIM
                      CLOSE REGCARGO
                      GO TO INICIO
                   ELSE
                      MOVE "ERRO AO TENTAR ABRIR ARQUIVO" TO W-MENSAGEM
                      PERFORM ROT-MENSAGEM THRU ROT-MENSAGEM-FIM
                      GO TO ROT-FIM
                ELSE
                    NEXT SENTENCE.
                    
       INC-LIMPAR-TELA.
                MOVE ZEROS  TO FORM-CODIGO
                MOVE ZEROS  TO FORM-TIPO-SALARIO-ID
                MOVE ZEROS  TO FORM-NIVEL-ID
                MOVE ZEROS  TO FORM-SALARIO-BASE
                MOVE SPACES TO FORM-DENOMINACAO
                MOVE SPACES TO FORM-TIPO-SALARIO-DESC
                MOVE SPACES TO FORM-NIVEL-DESCRICAO
                MOVE SPACES TO FORM-TIPO-RISCO-ID
                MOVE SPACES TO FORM-TIPO-RISCO-DESC
                MOVE SPACES TO FORM-SITUACAO-ID
                MOVE SPACES TO FORM-SITUACAO-DESC				   
                DISPLAY TELA.

       INC-CODIGO.
               ACCEPT (02, 9) FORM-CODIGO
               ACCEPT W-OPC FROM ESCAPE KEY
               IF FORM-CODIGO = ZEROS
                   MOVE "** CODIGO INVALIDO **" TO W-MENSAGEM
                   PERFORM ROT-MENSAGEM THRU ROT-MENSAGEM-FIM
                   GO TO INC-CODIGO
                ELSE 
                    GO TO INC-VERIFICACAO.

        INC-VERIFICACAO.
                READ REGCARGO
                IF ST-ERRO NOT = "23"
                    IF ST-ERRO = "00"
                       DISPLAY TELA
                       MOVE "- DEPARTAMENTO ENCONTRADO -" TO W-MENSAGEM
                       PERFORM ROT-MENSAGEM THRU ROT-MENSAGEM-FIM
                       IF W-ACAO = 2
                            GO TO INC-DENOMINACAO
                       ELSE
                            GO TO ROT-MENU 
                    ELSE
                       MOVE "ERRO LEITURA ARQUIVO DE DEPT" TO W-MENSAGEM
                       PERFORM ROT-MENSAGEM THRU ROT-MENSAGEM-FIM
                       GO TO ROT-FIM
                ELSE
                    GO TO INC-DENOMINACAO.

        INC-DENOMINACAO.
                ACCEPT (03, 14) FORM-DENOMINACAO
                ACCEPT W-OPC FROM ESCAPE KEY
                IF FORM-DENOMINACAO = SPACES
                    MOVE "- DENOMINAÇÃO INVÁLIDA -" TO W-MENSAGEM
                    PERFORM ROT-MENSAGEM THRU ROT-MENSAGEM-FIM
                    GO TO INC-DENOMINACAO
                ELSE
                    GO TO INC-TIPO-SALARIO-ID.

        INC-TIPO-SALARIO-ID.
            ACCEPT (04, 15) FORM-TIPO-SALARIO-ID
            ACCEPT W-OPC FROM ESCAPE KEY
            IF FORM-TIPO-SALARIO-ID = SPACES
                MOVE "TIPO SALARIO INVALIDO" TO W-MENSAGEM
                PERFORM ROT-MENSAGEM THRU ROT-MENSAGEM-FIM
                GO TO INC-TIPO-RISCO-ID
            ELSE
                GO TO INC-NIVEL-ID.
        
        INC-NIVEL-ID.
            ACCEPT (05, 08) FORM-NIVEL-ID
            ACCEPT W-OPC FROM ESCAPE KEY
            IF FORM-NIVEL-ID = ZEROS
                MOVE "NIVEL INVALIDO" TO W-MENSAGEM
                PERFORM ROT-MENSAGEM THRU ROT-MENSAGEM-FIM
                GO TO INC-NIVEL-ID
            ELSE
                GO TO INC-SALARIO-BASE.

        INC-SALARIO-BASE.
            ACCEPT (06, 15) FORM-SALARIO-BASE
            ACCEPT W-OPC FROM ESCAPE KEY
            IF FORM-SALARIO-BASE = ZEROS
                MOVE "SALARIO BASE INVALIDO" TO W-MENSAGEM
                PERFORM ROT-MENSAGEM THRU ROT-MENSAGEM-FIM
                GO TO INC-SALARIO-BASE
            ELSE
                GO TO INC-TIPO-RISCO-ID.

        INC-TIPO-RISCO-ID.
            ACCEPT (07, 13) FORM-TIPO-RISCO-ID
            ACCEPT W-OPC FROM ESCAPE KEY
            IF FORM-TIPO-RISCO-ID = SPACES
                MOVE "TIPO RISCO INVALIDO" TO W-MENSAGEM
                PERFORM ROT-MENSAGEM THRU ROT-MENSAGEM-FIM
                GO TO INC-TIPO-RISCO-ID
            ELSE
                GO TO INC-SITUACAO-ID.

        INC-SITUACAO-ID.
            ACCEPT (08, 11) FORM-SITUACAO-ID
            ACCEPT W-OPC FROM ESCAPE KEY
            IF FORM-TIPO-SALARIO-ID = SPACES
                MOVE "SITUACAO INVALIDA" TO W-MENSAGEM
                PERFORM ROT-MENSAGEM THRU ROT-MENSAGEM-FIM
                GO TO INC-SITUACAO-ID
            ELSE
                IF W-ACAO = 2
                    GO TO INC-ALTERAR
                ELSE   
                    GO TO INC-GRAVAR.
    
       INC-GRAVAR.
                WRITE CARGO
                IF ST-ERRO = "00" OR "02"
                      MOVE "*** DADOS GRAVADOS *** " TO W-MENSAGEM
                      PERFORM ROT-MENSAGEM THRU ROT-MENSAGEM-FIM
                      GO TO INC-LIMPAR-TELA.
                IF ST-ERRO = "22"
                      MOVE "CARGO JA EXISTE" TO W-MENSAGEM
                      PERFORM ROT-MENSAGEM THRU ROT-MENSAGEM-FIM
                      GO TO INC-LIMPAR-TELA
                ELSE
                      MOVE "ERRO NA GRAVACAO DO ARQUIVO DE CARGO"
                                                       TO W-MENSAGEM
                      PERFORM ROT-MENSAGEM THRU ROT-MENSAGEM-FIM
                      GO TO ROT-FIM.

       INC-EXCLUIR.
                DELETE REGCARGO RECORD
                IF ST-ERRO = "00"
                   MOVE "* REGISTRO EXCLUIDO *" TO W-MENSAGEM
                   PERFORM ROT-MENSAGEM THRU ROT-MENSAGEM-FIM
                   GO TO INC-LIMPAR-TELA.
                MOVE "ERRO NA EXCLUSAO DO REGISTRO " TO W-MENSAGEM
                PERFORM ROT-MENSAGEM THRU ROT-MENSAGEM-FIM
                GO TO ROT-FIM.

        INC-ALTERAR.
                REWRITE CARGO
                IF ST-ERRO = "00" OR "02"
                   MOVE "- REGISTRO ALTERADO -" TO W-MENSAGEM
                   PERFORM ROT-MENSAGEM THRU ROT-MENSAGEM-FIM
                   GO TO INC-LIMPAR-TELA.
                MOVE "ERRO AO EXCLUIR"  TO W-MENSAGEM
                PERFORM ROT-MENSAGEM THRU ROT-MENSAGEM-FIM
                GO TO ROT-FIM.


      ******************************************
      ** CONSULTA/ALTERAÇÃO E EXLUSÃO *
      ******************************************
       ROT-MENU.
                MOVE ZEROS TO W-ACAO.
                DISPLAY (23, 13)
                    "F1=NOVO REGISTRO F2=ALTERAR F3=EXCLUIR F4=CONSULTA"
                ACCEPT (23, 55) W-OPC
                ACCEPT W-OPC FROM ESCAPE KEY
                IF W-OPC = 02 OR W-OPC = 05
                    MOVE 1 TO W-ACAO
                    GO TO INC-LIMPAR-TELA.
                IF W-OPC = 03
                    MOVE 2 TO W-ACAO
                    GO TO INC-CODIGO.
                IF W-OPC = 04 
                    GO TO INC-EXCLUIR.
                GO TO ROT-MENU.

      **********************
      * ROTINA DE MENSAGEM *
      **********************
      *
       ROT-MENSAGEM.
                MOVE ZEROS TO W-CONT.
       ROT-MENSAGEM1.
               DISPLAY (22, 16) W-MENSAGEM.
       ROT-MENSAGEM2.
                ADD 1 TO W-CONT
                IF W-CONT < 2000
                   GO TO ROT-MENSAGEM2
                ELSE
                   DISPLAY (22, 16) LIMPA.
       ROT-MENSAGEM-FIM.
                EXIT.
       ROT-FIM.