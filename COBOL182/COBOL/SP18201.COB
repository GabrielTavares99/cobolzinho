       IDENTIFICATION DIVISION.
       PROGRAM-ID. P18201.
       AUTHOR. GABRIEL SILVA.
      *****************************************
      *    CADASTRO DE DEPARTAMENTO  *
      *****************************************
      *----------------------------------------------------------------
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
                         DECIMAL-POINT IS COMMA.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       SELECT CADDEPT ASSIGN TO DISK
                    ORGANIZATION IS INDEXED
                    ACCESS MODE  IS DYNAMIC
                    RECORD KEY   IS FORM-CODIGO
                    FILE STATUS  IS ST-ERRO
                    ALTERNATE RECORD KEY IS CHAVE2 = FORM-DENOMINACAO
                                                      WITH DUPLICATES.
      *-----------------------------------------------------------------
       DATA DIVISION.
       FILE SECTION.
       FD CADDEPT
               LABEL RECORD IS STANDARD
               VALUE OF FILE-ID IS "CADDEPT.DAT".
       01 REGDEPT.
                03 FORM-CODIGO           PIC 9(03).
                03 FORM-DENOMINACAO      PIC X(25).
                03 FORM-NIVEL            PIC X(20).
                03 FORM-SITUACAO         PIC X(12).
      *-----------------------------------------------------------------

      *-----------------------------------------------------------------
       WORKING-STORAGE SECTION.

       77 WS-MENSAGEM                      PIC X(99) VALUE SPACES.
       77 WS-CONT                          PIC 9(04) VALUE ZEROS.
       77 WS-OPCAO                         PIC X(01) VALUE SPACES.
       77 WS-LIMPA                         PIC X(50) VALUE SPACES.
       77 W-LEITURA                        PIC X(50) VALUE SPACES.
       77 ST-ERRO                          PIC X(02) VALUE "00".

      *
       01 TABELA-NIVEL.
          03 T1 PIC X(40) VALUE
          "DIRETORIA           ADMINISTRA��O       ".
          03 T2 PIC X(40) VALUE
          "COMERCIAL           INDUSTRIAL          ".
          03 T3 PIC X(40) VALUE
          "ESCRIT�RIO REGIONAL REPRESENTAC REGIONAL".
          03 T4 PIC X(40) VALUE
          "TERCEIRIZADO        P&D                 ".
          03 T5 PIC X(40) VALUE
          "EDUCACIONAL         N�O ESPECIALIZADO   ".

       01 TABAUX REDEFINES TABELA-NIVEL.
           03 TDA        PIC X(20) OCCURS 10 TIMES.
      *-----------------------------------------------------------------

       SCREEN SECTION.
       01  TELA.
           05  BLANK SCREEN.
           05  LINE 01  COLUMN 01
               VALUE  "                 CADASTRO DE DEPARTAMENT".
           05  LINE 01  COLUMN 41
               VALUE  "O".
           05  LINE 02  COLUMN 01
               VALUE  "CODIGO:".
           05  LINE 03  COLUMN 01
               VALUE  "DENOMINACAO:".
           05  LINE 04  COLUMN 01
               VALUE  "NIVEL:".
           05  LINE 05  COLUMN 01
               VALUE  "SITUACAO:".
           05  TXT-CODIGO
               LINE 02  COLUMN 09  PIC 9(03)
               USING  FORM-CODIGO.
           05  TXT-DENOMINACAO
               LINE 03  COLUMN 14  PIC X(25)
               USING  FORM-DENOMINACAO.
           05  TXT-NIVEL
               LINE 04  COLUMN 08  PIC X(20)
               USING  FORM-NIVEL.
           05  TXT-SITUACAO
               LINE 05  COLUMN 11  PIC X(12)
               USING  FORM-SITUACAO.

      *-----------------------------------------------------------------
       PROCEDURE DIVISION.
       INICIO.
      *
       INC-OP0.
           OPEN I-O CADDEPT
           IF ST-ERRO NOT = "00"
               IF ST-ERRO = "30"
                      OPEN OUTPUT CADDEPT
                      MOVE "*** ARQUIVO SENDO CRIADO... **"
                      TO WS-MENSAGEM
                      CLOSE CADDEPT
                      PERFORM ROT-MENSAGEM THRU ROT-MENSAGEM-FIM
                      GO TO INICIO
                   ELSE
                      MOVE "ERRO NA ABERTURA DO ARQUIVO"
                      TO WS-MENSAGEM
                      PERFORM ROT-MENSAGEM THRU ROT-MENSAGEM-FIM
                      GO TO ROT-FIM
                ELSE
      *>               MOVE "*** ARQUIVO J� FOI CRIADO ***"
      *>               TO WS-MENSAGEM
      *>               PERFORM ROT-MENSAGEM THRU ROT-MENSAGEM-FIM
                    NEXT SENTENCE.
       INC-001.
                MOVE ZEROS TO FORM-CODIGO
                MOVE SPACES TO FORM-DENOMINACAO FORM-NIVEL FORM-SITUACAO
                DISPLAY TELA.
       INC-002.
               ACCEPT TXT-CODIGO
               ACCEPT W-LEITURA FROM ESCAPE KEY
               IF FORM-CODIGO NOT GREATER ZEROS
                   MOVE "** CODIGO INVALIDO **" TO WS-MENSAGEM
                   PERFORM ROT-MENSAGEM THRU ROT-MENSAGEM-FIM
                   GO TO INC-002.


      *     COMPUTE A = B + C
      *
      **********************
      * ROTINA DE MENSAGEM *
      **********************
      *
       ROT-MENSAGEM.
                MOVE ZEROS TO WS-CONT.
       ROT-MENSAGEM1.
               DISPLAY (23, 12) WS-MENSAGEM.

       ROT-MENSAGEM2.
                ADD 1 TO WS-CONT
                IF WS-CONT < 3000
                   GO TO ROT-MENSAGEM2
                ELSE
                   DISPLAY (23, 12) WS-LIMPA.
       ROT-MENSAGEM-FIM.
                EXIT.
       ROT-FIM.
